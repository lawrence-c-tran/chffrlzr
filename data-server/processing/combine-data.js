const fs = require('fs');
const moment = require('moment');
const GeoJSON = require('geojson');
const TRIPS_DIRECTORY_PATH = '../trips/';

var count = 0;
var coords = [];
var lines = [];

const getWeekdayTimeIndex = time => {

    // Parse the string to individual parts
    const trip = time.replace('.json', '');
    const splitTripFileName = trip.split('--');
    const daySplit = splitTripFileName[0].split('-');
    const timeSplit = splitTripFileName[1].split('-');

    // Get individual values
    const year = daySplit[0];
    const month = daySplit[1];
    const day = daySplit[2];
    const hour = timeSplit[0];
    const minute = timeSplit[1];
    const second = timeSplit[2];

    const tripMoment = moment().year(year).month(month).day(day).hour(hour).minute(minute).second(second);
    const stringIndex = tripMoment.format('d') + tripMoment.format('HH');
    return Number(stringIndex);
}

fs.readdirSync(TRIPS_DIRECTORY_PATH).forEach(file => {
    const path = TRIPS_DIRECTORY_PATH + file;
    const timeIndex = getWeekdayTimeIndex(path);
    const data = JSON.parse(fs.readFileSync(path, 'utf8'));

    var lineString = {
        line: []
    };

    data.coords.forEach((coord, index) => {
        if (index % 50 === 0) {
            coord['time'] = timeIndex;
            coords.push(coord);
        }

        if (index % 30 === 0) {
            lineString.line.push([coord.lat, coord.lng]);
        }
    });

    lines.push(lineString);
    count++;
})

const pointData = GeoJSON.parse(coords, { Point: ['lat', 'lng'], include: ['speed', 'time'] });
const lineData = GeoJSON.parse(lines, { LineString: 'line' });
fs.writeFileSync('../data/speed-points-50.geojson', JSON.stringify(pointData, null, 2), 'utf-8');
fs.writeFileSync('../data/line-strings-30.geojson', JSON.stringify(lineData, null, 2), 'utf-8');

console.log('Processed ' + count + ' trips!');
