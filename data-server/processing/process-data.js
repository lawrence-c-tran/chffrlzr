const fs = require('fs');
const moment = require('moment');
const GeoJSON = require('geojson');
const stats = require("stats-analysis");
const TRIPS_DIRECTORY_PATH = '../trips/';
const HEATMAP_DIRECTORY = '../geojson/heatmap/';
const LINESTRING_DIRECTORY = '../geojson/linestring/';

var tripsIndex = {};
var speedsIndex = {};

const calcualteDataForTrip = trip => {
    var data = [];
    trip.coords.forEach( coord => {
        data.push(coord.speed);
    });

    const stdev = stats.stdev(data).toFixed(2) * 1;
    const mean = stats.mean(data).toFixed(2) * 1;
    const median = stats.median(data).toFixed(2) * 1;

    return {
        stdev, 
        median,
        mean
    }
};

const extractLineStringCoordinates = trip => {
    var coords = []

    trip.coords.forEach(coord => {
        coords.push([coord.lat, coord.lng]);
    });

    return coords;
};

const createGeoJSON = (trips, weekday, hour) => {

    var coords = [];
    var lineString = {
        line: []
    };

    // Interate each trip and combine coordinates
    trips.forEach(fileName => {
        const path = TRIPS_DIRECTORY_PATH + fileName;
        const trip = JSON.parse(fs.readFileSync(path, 'utf8'));
        const tripCoords = trip.coords;

        tripCoords.forEach((coord, index) => {
            if (index % 100 === 0) {
                coords.push(coord);
            }

            if (index % 15 === 0 && index === coord.index) {
                lineString.line.push([coord.lng, coord.lat]);
            }
        });
    });

    // Parse speed points
    const speedPointsGeoJSON = GeoJSON.parse(coords, { Point: ['lat', 'lng'], include: ['speed'] });
    const lineStringGeoJSON = GeoJSON.parse(lineString, { LineString: 'line' });

    // Write geojson to file
    const speedPointsFilePath = HEATMAP_DIRECTORY + weekday + '-' + hour + '.geojson';
    const lineStringFilePath = LINESTRING_DIRECTORY + weekday + '-' + hour + '.geojson';

    console.log(speedPointsFilePath);

    fs.writeFileSync(speedPointsFilePath, JSON.stringify(speedPointsGeoJSON, null, 2), 'utf-8');
    fs.writeFileSync(lineStringFilePath, JSON.stringify(lineStringGeoJSON, null, 2), 'utf-8');
};

const getWeekdayHourTime = time => {
    const stringIndex = time.format('d') + time.format('HH');
    return Number(stringIndex);
};

const indexTrip = (fileName, data) => {

    // Parse the string to individual parts
    const trip = fileName.replace('.json', '');
    const splitTripFileName = trip.split('--');
    const daySplit = splitTripFileName[0].split('-');
    const timeSplit = splitTripFileName[1].split('-');

    // Get individual values
    const year = daySplit[0];
    const month = daySplit[1];
    const day = daySplit[2];
    const hour = timeSplit[0];
    const minute = timeSplit[1];
    const second = timeSplit[2];

    // Create moment
    const tripMoment = moment().year(year).month(month).day(day).hour(hour).minute(minute).second(second);
    const weekday = tripMoment.format('d');
    const hourFormat = tripMoment.format('HH');
    const weekdayHourTime = getWeekdayHourTime(tripMoment);

    if (tripsIndex[weekday] === undefined) {
        tripsIndex[weekday] = {}
    }

    if (tripsIndex[weekday][hourFormat] === undefined) {
        tripsIndex[weekday][hourFormat] = []
    }

    // Index trip name 
    tripsIndex[weekday][hourFormat].push(fileName);
    speedsIndex[''+weekdayHourTime] = calcualteDataForTrip(data);
};

const processTripFiles = () => {
    var count = 0;

    // Index trips by time during the week
    fs.readdirSync(TRIPS_DIRECTORY_PATH).forEach(file => {
        const path = TRIPS_DIRECTORY_PATH + file;
        const data = JSON.parse(fs.readFileSync(path, 'utf8'));

        createGeoJSON(trip, data);
        indexTrip(file, data);
        count++;
    })

    // Iterate index and create GeoJSON
    for (var i = 0; i < 7; i++) {
        for (var j = 0; j < 24; j++) {
            var k = j;
            if (k < 10) {
                k = '0' + k;
            }
            const trips = tripsIndex[i][k];
            if (trips !== undefined) {
                createGeoJSON(trips, i, k);
            }
        }
    }

    fs.writeFileSync('../data/trips-index.json', JSON.stringify(tripsIndex, null, 2), 'utf-8');
    fs.writeFileSync('../data/speed-analysis.json', JSON.stringify(speedsIndex, null, 2), 'utf-8');
    console.log('Processed ' + count + ' trips!');
};

// Run
processTripFiles();
