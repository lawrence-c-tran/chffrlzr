const app = require('express')();
const bodyParser = require('body-parser');
const fs = require('fs');
const moment = require('moment');
const tripsIndex = JSON.parse(fs.readFileSync('./data/trips-index.json', 'utf8'));
const HEATMAP_DIRECTORY = './geojson/heatmap/';
const LINESTRING_DIRECTORY = './geojson/linestring/';

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', function (req, res) {
    res.send(sunday);
})

// GET trips if any for given time
app.get('/trips', function (req, res) {

    // Get required parameters
    const sort = req.query['sort'];
    const type = req.query['type'];
    const startTime = req.query['startTime'];
    const endTime = req.query['endTime'];
    var payload = {
        name: '',
        geojson: {}
    };

    // Sort by weekday
    if (sort === 'weekday') {
        const startMoment = moment(startTime);

        if (startMoment !== null) {
            const weekday = startMoment.format('d');
            const hour = startMoment.format('HH');

            // Trips exist
            if (tripsIndex[weekday][hour] !== undefined) {
                var name = weekday + '-' + hour;
                var filePath = weekday + '-' + hour + '.geojson';
                if (type === 'lines') {
                    name = name + '-lines';
                    filePath = LINESTRING_DIRECTORY + filePath;
                }
                else {
                    name = name + '-heatmap';
                    filePath = HEATMAP_DIRECTORY + filePath;
                }
                payload.name = name;
                payload.geojson = JSON.parse(fs.readFileSync(filePath, 'utf8'));

                console.log('Fetched: ' + filePath);
                res.status(200).send(payload);
            }
            else {
                res.status(200).send(payload);
            }
        }
        else {
            res.sendStatus(504);
        }
    }
    else {
        res.sendStatus(400);
    }
})

app.listen(8000, () => console.log('App listening on port 8000!'));
