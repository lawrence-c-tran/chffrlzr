#chffrlzr data server

This project contains two things:
    - Processing scrips parse and analyze raw trip data.
    - A small REST api that returns trip data for a given time range.

##Processing

`processing/process-data.j`
Reads the contents of each trip file and creates a GeoJSON of the coordinate points and a line string.
This script also creates two files:
    - `data/speed-analysis.json` — Contains the mean, median, and standard deviation of each trip's speed.
    - `data/trips-index.json` — An index of trips ordered by weekday and hour.

`processing/combine-data.s`
Combines all coordinates from all trips into a single GeoJSON for display purposes. Every 50th coordinate is sampled for heatmap. Every 30th for line strings.
    - `data/speed-points-50.geojson` - Combine coordinates, with speed and time data, of all trips.
    - `data/line-strings-30.geojson` - Combined linestrings of all trips.

##Trips API

Small REST API that returns a GeoJSON for params.

```
npm install
npm start
```

```
var params = {
    startTime
    endTime
    sort
}
```

Sample API call
```
http://localhost:8000/trips?startTime=2018-04-22T18:39:00.000Z&endTime=2018-04-22T18:59:00.000Z&sort=weekday
```

##Room for improvement
    - Use a database like MongoDB to index the data insted of json files.
    - Add streaming to the API for large datasets.
    - Add more sorting options.
