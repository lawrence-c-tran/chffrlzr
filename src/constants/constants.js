// Mapbox acess token
export const MAPBOX_TOKEN = 'pk.eyJ1IjoibGF3cmVuY2VjdHJhbiIsImEiOiJjamdjdmRxZzFhM3NzMzNtZHcybzFyajB0In0.odnx7_C3ORBbOTshFy8tjw';

// Rate of each time tick
export const TIMER_RATE = 250;

// Minutes in each time tick
export const TIME_RATE = 60;

// API end poing
export const API_END_POINT = 'http://localhost:8000/trips'; // Used localhost during development
