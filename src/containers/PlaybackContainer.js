import { connect } from 'react-redux';
import { toggleDoubleSpeed, togglePlayback, toggleSettings, incrementTime } from '../actions';
import PlaybackBar from '../components/PlaybackBar';

const mapStateToProps = state => ({
    isDoubleSpeed: state.playback.isDoubleSpeed,
    isPlaying: state.playback.isPlaying,
    isShowingSettings: state.playback.isShowingSettings
});

const mapDispatchToProps = dispatch => ({
    testIncrement: () => dispatch(incrementTime()),
    onToggleDoubleSpeed: () => dispatch(toggleDoubleSpeed()),
    onTogglePlayback: () => dispatch(togglePlayback()),
    onToggleSettings: () => dispatch(toggleSettings())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PlaybackBar)
