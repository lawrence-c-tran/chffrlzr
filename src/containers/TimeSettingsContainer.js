import { connect } from 'react-redux';
import { updateHour, updateWeekday, toggleAutoplay, toggleCombined } from '../actions';
import TimeSettingsPanel from '../components/TimeSettingsPanel';

const mapStateToProps = (state, ownProps) => ({
    isShowingSettings: ownProps.isShowingSettings,
    time: state.info.time,
    isAutoplay: state.info.isAutoplay,
    isCombined: state.info.isCombined
});

const mapDispatchToProps = dispatch => ({
    onUpdateWeekday: weekday => dispatch(updateWeekday(weekday)),
    onUpdateHour: hour => dispatch(updateHour(hour)),
    onToggleAutoplay: () => dispatch(toggleAutoplay()),
    onToggleCombined: () => dispatch(toggleCombined())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TimeSettingsPanel)
