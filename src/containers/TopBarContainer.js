import { connect } from 'react-redux';
import TopBar from '../components/TopBar';

const mapStateToProps = state => ({
    isFirstPlay: state.info.isFirstPlay
});

export default connect(
    mapStateToProps
)(TopBar)
