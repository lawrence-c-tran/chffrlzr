import { connect } from 'react-redux';
import { incrementTime } from '../actions';
import App from '../components/App';

const mapStateToProps = state => ({
    isDoubleSpeed: state.playback.isDoubleSpeed,
    isPlaying: state.playback.isPlaying,
    isShowingSettings: state.playback.isShowingSettings,
});

const mapDispatchToProps = dispatch => ({
    onIncrementTime: time => dispatch(incrementTime(time))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)
