import { connect } from 'react-redux';
import { fetchData } from '../actions';
import Map from '../components/Map';

const mapStateToProps = state => ({
    time: state.info.time,
    weekdayTimeIndex: state.info.weekdayTimeIndex,
    sort: state.info.sort,
    mapFilter: state.info.mapFilter
});

const mapDispatchToProps = dispatch => ({
    onFetchData: params => dispatch(fetchData(params))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Map)
