import { connect } from 'react-redux';
import { updateWeekday } from '../actions';
import MainInfoPanel from '../components/MainInfoPanel';

const mapStateToProps = (state, ownProps) => ({
    time: state.info.time,
    analysis: state.info.analysis
});

const mapDispatchToProps = dispatch => ({
    onUpdateWeekday: weekday => dispatch(updateWeekday(weekday)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MainInfoPanel)
