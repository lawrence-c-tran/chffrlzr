import React, { Component } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const ToolTipSlider = createSliderWithTooltip(Slider);

const Wrapper = styled.div`
    width: 85%;
    height: 30px;
    margin-left: auto;
    margin-right: auto;
`;

const marks = {
    0: <strong>S</strong>,
    1: <strong>M</strong>,
    2: <strong>T</strong>,
    3: <strong>W</strong>,
    4: <strong>Th</strong>,
    5: <strong>F</strong>,
    6: <strong>S</strong>,
};

const tipFormatter = value => {
    return moment().day(value).format('dddd');
};

let WeekdaySlider = class WeekdaySlider extends Component {

    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    onChange(value) {
        this.props.onUpdateWeekday(value);
    }

    render() {
        return (
            <Wrapper>
                <ToolTipSlider
                    max={ 6 }
                    marks={ marks }
                    step={ null }
                    onChange={ this.onChange }
                    tipFormatter={ tipFormatter }
                    handleStyle={ [{ backgroundColor: 'rgba(255, 169, 214', borderColor: 'white' }] }
                    included={ false }
                    value={ Number(this.props.time.format('d')) }
                />
            </Wrapper>
        );
    }
}

export default WeekdaySlider;
