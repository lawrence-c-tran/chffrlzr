import React, { Component } from 'react';
import styled from 'styled-components';

const Background = styled.button`
    height: 50px;
    width: 50px;
    border-radius: 100%;
    background-color: #18D8F0;
`;

const Icon = styled.img`
    margin: auto auto auto auto;
`;

let CircleButton = class CircleButton extends Component {

    render() {
        return (
            <Background>
                <Icon src={ this.props.image } />
            </Background>
        );
    }
}

export default CircleButton;
