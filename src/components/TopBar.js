import React, { Component } from 'react';
import styled from 'styled-components';
import MainInfoContainer from '../containers/MainInfoContainer';
import Instructions from '../components/Instructions';

const Wrapper = styled.div`
    position: absolute;
    top: 3%;
    left: 10%;
    width: 240px;
`;

const InstructionsWrapper = styled.div`
    padding-top: 8px;
    padding-left: 8px;
`;

let TopBar = class TopBar extends Component {
    render() {
        return (
            <Wrapper>
                <MainInfoContainer />
                <InstructionsWrapper>
                    <Instructions isFirstPlay={ this.props.isFirstPlay } />
                </InstructionsWrapper>
            </Wrapper>
        );
    }
}

export default TopBar;

