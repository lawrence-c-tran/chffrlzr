import React, { Component } from 'react';
import { TIMER_RATE, TIME_RATE } from '../constants/constants';
import MapContainer from '../containers/MapContainer';
import TopBarContainer from '../containers/TopBarContainer';
import TimeSettingsContainer from '../containers/TimeSettingsContainer';
import PlaybackContainer from '../containers/PlaybackContainer';

let App = class App extends Component {

  constructor(props) {
    super(props);

    this.state = { ...this.props, timerIds: [] };
  }

  static getDerivedStateFromProps(nextProps, prevState) {

    // Pause
    if (nextProps.isPlaying === false) {
      prevState.timerIds.forEach(function (val, index) {
        clearInterval(val);
      });
      return { ...prevState, isPlaying: false, timerIds: [] };
    }

    // Stop interval if changing rate
    if (nextProps.isDoubleSpeed !== prevState.isDoubleSpeed) {
      prevState.timerIds.forEach(function (val, index) {
        clearInterval(val);
      });
    }

    // Start interval
    let rate = (nextProps.isDoubleSpeed) ? 0.4 * TIMER_RATE : TIMER_RATE;
    let newTimerId = setInterval(
      () => prevState.onIncrementTime(TIME_RATE),
      rate
    );

    var newTimerIds = Array.from(prevState.timerIds);
    newTimerIds.push(newTimerId);

    let newS = { ...prevState, isDoubleSpeed: nextProps.isDoubleSpeed, isPlaying: true, timerIds: newTimerIds };
    return newS;
  }

  render() {
    return (
      <div>
        <MapContainer />
        <TopBarContainer />
        <TimeSettingsContainer isShowingSettings={this.props.isShowingSettings} />
        <PlaybackContainer />
      </div>
    );
  }
}

export default App;
