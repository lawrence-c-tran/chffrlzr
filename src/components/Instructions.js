import React, { Component } from 'react';
import styled from 'styled-components';

const Header = styled.h1`
    font-size: 18px;
    font-weight: bold;
    color: '#a9a9a9';
`;

let Instructions = class Instructions extends Component {

    render() {
        if (this.props.isFirstPlay === true) {
            return (
                <Header>
                    <span role={ 'img' } aria-label={ 'car' }>🚘</span> Press play to begin! <span role={ 'img' } aria-label={ 'car' }>🚘</span>
                </Header>
            );
        }
        else {
            return null;
        }
    }
}

export default Instructions;
