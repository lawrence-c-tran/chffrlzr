import React from 'react';
import mapboxgl from 'mapbox-gl';
import { MAPBOX_TOKEN } from '../constants/constants';

const speedPoints = require('../data/speed-points-50.json');

// Setup Mapbox GL
mapboxgl.accessToken = MAPBOX_TOKEN;
var map;

let Map = class Map extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            lat: 37.70272,
            lng: -122.215248,
            zoom: 8.5,
            mapFilter: this.props.mapFilter
        };
    }

    componentDidMount() {
        const { lng, lat, zoom } = this.state;

        map = new mapboxgl.Map({
            container: this.mapContainer,
            style: 'mapbox://styles/mapbox/light-v9',
            center: [lng, lat],
            zoom
        });

        map.on('move', () => {
            const { lng, lat } = map.getCenter();

            this.setState({
                lng: lng.toFixed(4),
                lat: lat.toFixed(4),
                zoom: map.getZoom().toFixed(2)
            });
        });

        map.on('load', () => {

            map.addSource('speed-points', {
                type: 'geojson',
                data: speedPoints
            });

            map.addLayer({
                id: 'speed-points-layer',
                type: 'circle',
                source: 'speed-points',
                filter: this.state.mapFilter,
                paint: {
                    'circle-radius': {
                        'base': 3,
                        'stops': [[12, 4], [22, 200]]
                    },
                    'circle-color': [
                        'interpolate',
                        ['linear'],
                        ['number', ['get', 'speed']],
                        0, '#FBE1E7',
                        5, '#FFB0CD',
                        15, '#FFA9D7',
                        20, '#A4C8E4',
                        60, '#00E3FF'
                    ],
                }
            });
        });
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.mapFilter !== prevState.mapFilter) {
            map.setFilter('speed-points-layer', nextProps.mapFilter);
            return { ...prevState, mapFilter: nextProps.mapFilter };
        }
        return null;
    }

    render() {
        return (
            <div ref={el => this.mapContainer = el} className="absolute top right left bottom" />
        );
    }
}

export default Map;
