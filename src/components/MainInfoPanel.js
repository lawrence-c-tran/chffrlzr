import React, { Component } from 'react';
import styled from 'styled-components';
import legend from '../assets/legend.png';

const Background = styled.div`
    padding: 10px;
    background-color: rgba(255, 255, 255, .55);
    border-radius: 10px;
`;

const Title = styled.h1`
    font-weight: bold;
    font-size: 26px;
    margin-bottom: 4px;
`;

const Subtite = styled.h2`
    font-size: 20px;
    margin-bottom: 8px;
`;

const ValueSpan = styled.span`
    color: green;
`;

const Legend = styled.div`
    margin-top: 16px;
`

const Icon = styled.img`
    margin: auto auto auto auto;
`;

let MainPanel = class MainPanel extends Component {

    render() {
        return (
            <Background>
                <Title>{ this.props.time.format('dddd') }</Title>
                <Subtite>{ this.props.time.format('h a')}</Subtite>

                Average <ValueSpan>{ this.props.analysis.mean}</ValueSpan><br />
                Median <ValueSpan>{ this.props.analysis.median}</ValueSpan><br />
                Deviation <ValueSpan>{ this.props.analysis.stdev}</ValueSpan><br />

                <Legend>
                    <Icon src= { legend } />
                </Legend>

            </Background>
        );
    }
}

export default MainPanel;
