import React, { Component } from 'react';
import styled from 'styled-components';
import doubleSpeedIcon from '../assets/double-speed-icon.png';
import normalSpeedIcon from '../assets/normal-speed-icon.png'
import playIcon from '../assets/play-icon.png';
import pauseIcon from '../assets/pause-icon.png';
import timeSettingsIcon from '../assets/time-settings-icon.png';
import closeIcon from '../assets/close-icon.png';

const Background = styled.div`
    position: absolute;
    bottom: 5%;
    left: 50%;
    transform: translate(-50%, 0);
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding-left: 10px;
    padding-right: 10px;
    margin-left: auto;
    margin-right: auto;
    height: 44px;
    width: 280px;
    border-radius: 30px;
    background-color: #18D8F0;
`;

const Item = styled.button`
    width: 44px;
    height: 44px;
`;

const Icon = styled.img`
    margin: auto auto auto auto;
`;

let PlaybackBar = class PlaybackBar extends Component {

    render() {
        return (
            <Background>
                <Item onClick={ this.props.onToggleDoubleSpeed }>
                    <Icon src={ (this.props.isDoubleSpeed) ? normalSpeedIcon : doubleSpeedIcon } />
                </Item>
                <Item onClick={ this.props.onTogglePlayback }>
                    <Icon src={ (this.props.isPlaying) ? pauseIcon : playIcon } />
                </Item>
                <Item onClick={ this.props.onToggleSettings }>
                    <Icon src={ (this.props.isShowingSettings) ? closeIcon :timeSettingsIcon } />
                </Item>
            </Background>
        );
    }
}

export default PlaybackBar;
