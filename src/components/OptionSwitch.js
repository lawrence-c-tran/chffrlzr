import React, { Component } from 'react';
import styled from 'styled-components';
import Switch from 'rc-switch';
import 'rc-switch/assets/index.css';

const Wrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 90%;
    margin-left: auto;
    margin-right: auto;
`;

let OptionSwitch = class OptionSwitch extends Component {
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    onChange() {
        this.props.onChange();
    }

    render() {
        return (
            <Wrapper>
                { this.props.title }
                <Switch
                    style = { {color: 'blue'} }
                    checked={ this.props.isChecked }
                    onChange={ this.onChange }
                />
            </Wrapper>
        );
    }
}
export default OptionSwitch;
