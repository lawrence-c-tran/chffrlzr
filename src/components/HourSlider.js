import React, { Component } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const ToolTipSlider = createSliderWithTooltip(Slider);

const Wrapper = styled.div`
    width: 85%;
    height: 30px;
    margin-left: auto;
    margin-right: auto;
`;

const marks = {
    0: <strong>12am</strong>,
    12: <strong>12pm</strong>,
    23: <strong>11pm</strong>,
};

const tipFormatter = value => {
    return moment().hour(value).minute(0).format('h a');
};

let HourSlider = class HourSlider extends Component {

    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    onChange(value) {
        this.props.onUpdateHour(value);
    }

    render() {
        return (
            <Wrapper>
                <ToolTipSlider
                    max={ 23 }
                    marks={ marks }
                    onChange={ this.onChange }
                    tipFormatter = { tipFormatter }
                    handleStyle={ [{ backgroundColor: 'rgba(255, 169, 214', borderColor: 'white' }] }
                    included={ false }
                    value={ Number(this.props.time.format('H')) }
                />
            </Wrapper>
        );
    }
}

export default HourSlider;
