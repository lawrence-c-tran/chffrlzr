import React, { Component } from 'react';
import styled from 'styled-components';
import HourSlider from './HourSlider';
import WeekdaySlider from './WeekdaySlider';
import OptionSwitch from './OptionSwitch';

const Background = styled.div`
    position: absolute;
    bottom: 15%;
    left: 50%;
    transform: translate(-50%, 0);
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    padding-top: 8px;
    padding-bottom: 8px;
    margin-left: auto;
    margin-right: auto;
    height: 190px;
    width: 320px;
    background-color: rgba(24, 192, 240, .8);
    border-radius: 10px;
`;

let TimeSettings = class TimeSettings extends Component {

    render() {
        if (this.props.isShowingSettings === true) {
            return (
                <Background>
                    <HourSlider
                        time={ this.props.time }
                        onUpdateHour={ this.props.onUpdateHour }
                    />
                    <WeekdaySlider
                        time={ this.props.time }
                        onUpdateWeekday={ this.props.onUpdateWeekday }
                    />
                    <OptionSwitch
                        title='autoplay next day'
                        isChecked={ this.props.isAutoplay }
                        onChange={ this.props.onToggleAutoplay }
                    />
                    <OptionSwitch
                        title='combine map data'
                        isChecked={ this.props.isCombined }
                        onChange={ this.props.onToggleCombined }
                    />
                </Background>
            );
        }
        else {
            return null;
        }
    }
}

export default TimeSettings;
