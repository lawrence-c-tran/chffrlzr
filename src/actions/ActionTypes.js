// Time
export const INCREMENT_TIME = 'INCREMENT_TIME';

// Info
export const SHOW_ALL_MAP = 'SHOW_ALL_MAP';
export const CLEAR_MAP = 'CLEAR_MAP';

// Data
export const REQUEST_DATA = 'REQUEST_DATA';
export const RECEIVE_DATA = 'REVEIVE_DATA';
export const FETCH_DATA = 'FETCH_DATA';

// Time settings
export const UPDATE_HOUR = 'UPDATE_HOUR';
export const UPDATE_WEEKDAY = 'UPDATE_WEEKDAY';
export const TOGGLE_AUTOPLAY = 'TOGGLE_AUTOPLAY';
export const TOGGLE_COMBINED = 'TOGGLE_COMBINED';

// Playback controls
export const TOGGLE_DOUBLE_SPEED = 'TOGGLE_DOUBLE_SPEED';
export const TOGGLE_PLAYBACK = 'TOGGLE_PLAYBACK';
export const TOGGLE_SETTINGS = 'TOGGLE_SETTINGS';
