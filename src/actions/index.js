import * as types from './ActionTypes';
import axios from 'axios';
import { API_END_POINT } from '../constants/constants';

export const incrementTime = increment => ({
    type: types.INCREMENT_TIME,
    payload: {
        increment
    }
});

export const requestData = () => ({
    type: types.RECEIVE_DATA
});

export const receiveData = (name, geojson) => ({
    type: types.RECEIVE_DATA,
    payload: {
        receivedAt: Date.now(),
        name,
        geojson
    }
})

export const fetchData = params => (
    (dispatch, getState) => {
        dispatch(requestData());
        return axios(API_END_POINT, { params })
            .then(function (response) {
                const data = response.data;
                if (data.name !== '') {
                    dispatch(receiveData(data.name, data.geojson));
                }
            })
            .catch(function (error) {
                console.log(error);
            })
    }
);

export const updateWeekday = newWeekday => ({
    type: types.UPDATE_WEEKDAY,
    payload: {
        weekday: newWeekday
    }
});

export const updateHour = newHour => ({
    type: types.UPDATE_HOUR,
    payload: {
        time: newHour
    }
});

export const showAllMap = () => ({
    type: types.SHOW_ALL_MAP
});

export const clearMap = () => ({
    type: types.CLEAR_MAP
});

export const toggleAutoplay = () => ({
    type: types.TOGGLE_AUTOPLAY
});

export const toggleCombined = () => ({
    type: types.TOGGLE_COMBINED
});

export const toggleDoubleSpeed = () => ({
    type: types.TOGGLE_DOUBLE_SPEED
});

export const togglePlayback = () => ({
    type: types.TOGGLE_PLAYBACK
});

export const toggleSettings = () => ({
    type: types.TOGGLE_SETTINGS
});
