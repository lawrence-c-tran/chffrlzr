import moment from 'moment';
const speedAnalysis = require('../data/speed-analysis.json');

export const getIncrementedTime = (time, increment, autoplay) => {
    let newTime = time.clone().add(increment, 'm');

    if (autoplay === false && !newTime.isSame(time, 'day')) {
        return newTime.clone().day(time.format('d'));
    }
    else {
        return newTime;
    }
};

export const getTimeForWeekday = (weekday) => {
    return moment().clone().day(weekday).hour(0).minute(0).second(0).millisecond(0);
};

export const getTimeForHour = (time, newHour) => {
    return time.clone().hour(newHour).minute(0).second(0).millisecond(0);
};

export const getWeekdayHourTime = time => {
    const stringIndex = time.format('d') + time.format('HH');
    return Number(stringIndex);
};

export const getAnalysisForWeekdayHour = (weekdayHour, state) => {
    const weekdayHourString = String(weekdayHour);
    const analysis = speedAnalysis[weekdayHourString];
    if (analysis !== undefined) {
        return analysis;
    }
    else {
        return state.analysis;
    }
};

export const getStateForIncrementTime = (state, payload) => {
    const newTime = getIncrementedTime(state.time, payload.increment, state.isAutoplay);
    const newWeekdayHourTime = getWeekdayHourTime(newTime);
    const newMapFilter = getMapFilter(newWeekdayHourTime, state.isCombined);
    const newAnalysis = getAnalysisForWeekdayHour(newWeekdayHourTime, state);
    return { ...state, time: newTime, weekdayHourTime: newWeekdayHourTime, mapFilter: newMapFilter, analysis: newAnalysis };
};

export const getStateForUpdateWeekday = (state, payload) => {
    const newTime = getTimeForWeekday(payload.weekday);
    const newWeekdayHourTime = getWeekdayHourTime(newTime);
    const newMapFilter = getMapFilter(newWeekdayHourTime, state.isCombined);
    const newAnalysis = getAnalysisForWeekdayHour(newWeekdayHourTime, state);
    return { ...state, time: newTime, weekdayHourTime: newWeekdayHourTime, mapFilter: newMapFilter, analysis: newAnalysis };
};

export const getStateForUpdateHour = (state, payload) => {
    const newTime = getTimeForHour(state.time, payload.time);
    const newWeekdayHourTime = getWeekdayHourTime(newTime);
    const newMapFilter = getMapFilter(newWeekdayHourTime, state.isCombined);
    const newAnalysis = getAnalysisForWeekdayHour(newWeekdayHourTime, state);
    return { ...state, time: newTime, weekdayHourTime: newWeekdayHourTime, mapFilter: newMapFilter, analysis: newAnalysis };
};

export const getMapFilter = (weekdayHourTime, combined) => {
    const combinedFilter = ['<=', ['number', ['get', 'time']], weekdayHourTime];
    const hourFilter = ['==', ['number', ['get', 'time']], weekdayHourTime];
    return (combined) ? combinedFilter : hourFilter;
};

export const mapAllFilter = ['>=', ['number', ['get', 'time']], 0];
export const mapClearFilter = ['==', ['number', ['get', 'time']], -1];
