import { combineReducers } from 'redux';
import info from './info';
import data from './data';
import playback from './playback';

export default combineReducers({
    info,
    data,
    playback
})
