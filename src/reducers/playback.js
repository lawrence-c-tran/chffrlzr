import { TOGGLE_DOUBLE_SPEED, TOGGLE_PLAYBACK, TOGGLE_SETTINGS } from '../actions/ActionTypes';

const playback = (state = {
    isDoubleSpeed: false,
    isPlaying: false,
    isShowingSettings: false,
    timeStart: 0
}, { type, payload }) => {
    switch (type) {
        case TOGGLE_DOUBLE_SPEED:
            return { ...state, isDoubleSpeed: !state.isDoubleSpeed };
        case TOGGLE_PLAYBACK:
            return { ...state, isPlaying: !state.isPlaying };
        case TOGGLE_SETTINGS:
            return { ...state, isShowingSettings: !state.isShowingSettings };
        default:
            return state;
    }
}

export default playback
