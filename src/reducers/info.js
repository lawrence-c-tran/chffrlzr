import { INCREMENT_TIME, UPDATE_WEEKDAY, UPDATE_HOUR, 
         TOGGLE_AUTOPLAY, TOGGLE_COMBINED, TOGGLE_PLAYBACK, SHOW_ALL_MAP, CLEAR_MAP } from '../actions/ActionTypes';
import { getTimeForWeekday, getStateForIncrementTime, getStateForUpdateWeekday,
         getStateForUpdateHour, getMapFilter, mapAllFilter, mapClearFilter } from '../utils/timeUtil';

const info = (state = {
    time: getTimeForWeekday(0),
    weekdayHourTime: 0,
    averageSpeed: 0.0,
    isFirstPlay: true,
    isAutoplay: true,
    isCombined: true,
    mapFilter: mapClearFilter,
    sort: 'weekday',
    tripsToPresent: [],
    analysis: {
        mean: '',
        stdev: '',
        mediuam: '',
    } 
}, { type, payload }) => {

    switch (type) {
        case INCREMENT_TIME:
            return getStateForIncrementTime(state, payload);

        case UPDATE_WEEKDAY:
            return getStateForUpdateWeekday(state, payload);

        case UPDATE_HOUR:
            return getStateForUpdateHour(state, payload);

        case TOGGLE_PLAYBACK:
            if (state.isFirstPlay === true) {
                return { ...state, isFirstPlay: false };
            }
            else {
                return state;
            }

        case TOGGLE_AUTOPLAY:
            return { ...state, isAutoplay: !state.isAutoplay };

        case TOGGLE_COMBINED:
            const isCombined = !state.isCombined;
            return { ...state, isCombined: isCombined, mapFilter: getMapFilter(this.weekdayHourTime, isCombined) };

        case SHOW_ALL_MAP:
            return { ...state, mapFilter: mapAllFilter };

        case CLEAR_MAP:
            return { ...state, mapFilter: mapClearFilter };

        default:
            return state;
    }
}

export default info 
