import { REQUEST_DATA, RECEIVE_DATA } from '../actions/ActionTypes';

const data = (state = {
    isFetching: false,
    shouldDisplay: [],
    geoJSON: []
}, { type, payload }) => {

    switch (type) {

        case REQUEST_DATA:
            return { ...state, isFetching: true };

        case RECEIVE_DATA:
            if (payload !== undefined) {
                if (!state.cache.hasOwnProperty(payload.name)) {
                    var newCache = { ...state.cache };
                    newCache[payload.name] = payload.geojson;

                    var newDisplayedData = Array.from(state.displayedData);
                    newDisplayedData.push(payload.name);

                    return { ...state, isFetching: false, displayedData: newDisplayedData, cache: newCache };
                }
                else {
                    return { ...state, isFetching: false };
                }
            }
            return state;

        default:
            return state;
    }
}

export default data
