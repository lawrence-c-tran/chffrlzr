#chffrlzr - The chffr visualizer

![Alt text](./images/chffrlzr.gif)

A visualizer tool for chffr trip data.

```
npm install
npm start
```

##Design

I decided to visualize the data by each hour of the week. This visualization is indicative of people's driving patterns.

Here is the initial design I made in Sketch. The original sketch file is available `design/chffrlzr-ui.sketch`.

![Alt text](./images/mockup.png)

##Figuring out how to handle data

The raw data contains 527 files totaling about 73.4 mb. When converted to GeoJSON, this data becomes 159.1mb (GeoJSON contains a lot of boilerplate). This is too much to load on startup for the client. I want my UI to be fast and responsive. I decided to create a small REST API that would respond with data for a given time. This allows me to scale-up while keeping my front-end lightweight.

However, while I solved load times, I came across map lag. Each request happens about once per second, and responses range from 100kb to 2mb. Since each data point was taken one second apart, a lot of similar data exists across the set. For analysis purposes these small differences are important, but for displaying they are not. So, I decided to thin out the data by only using 1 point for every 10 or 50 points. The results were great!

Here are images of the thinned map.

###raw data - 159.1mb

###Every 10 - 16mb
![Alt text](./images/10.png)

###Every 20 - 8mb
![Alt text](./images/20.png)


###Every 50 - 3.3mb
![Alt text](./images/50.png)

###Every 100 - 1.7mb
![Alt text](./images/100.png)

I was not expecting such a drastic reduction in file size with good map fidelity. For the sake of this project, I used `Every 50` and included the entire dataset on the client. Startup time for the app was still fast and map rendering had zero lag.

The code for switching to an API design still remains.

#chffrlzr data server

This sub-project contains two things:

* Processing scripts that parse and analyze raw trip data.
* A small REST API that returns trip data for a given time range.

##Processing

Originally, I wanted lines along with points on the map, so I generated data for it. However, lines with points made the map look ugly, so it was removed.

`processing/process-data.j`
Creates a point and a line GeoJSON for each trip and these two files:

* `data/speed-analysis.json` Contains the mean, median, and standard deviation of each trip's speed.
* `data/trips-index.json` An index of trips ordered by weekday and hour.

`processing/combine-data.s`
Combines all coordinates from all trips into a single GeoJSON. Every 50th coordinate is sampled for points. Every 30th for lines.

* `data/speed-points-50.geojson` Combine coordinates, with speed and time data, of all trips.
* `data/line-strings-30.geojson` Combined linestrings of all trips.

##Trips API

Small REST API that returns a GeoJSON for params.

```
cd data-server
npm install
npm start
```

```
var params = {
    startTime
    endTime
    sort
}
```

Sample API call
```
http://localhost:8000/trips?startTime=2018-04-22T18:39:00.000Z&endTime=2018-04-22T18:59:00.000Z&sort=weekday
```

##Room for Improvement
* Use a database like MongoDB to index the data instead of json files.
* Add streaming to the API for large datasets.
* Add more sorting options.
